using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    [TestFixture]
    public class TestesCalculadoraComissao
    {
        [Test]
        public void Venda_de_10000_retorna_comissao_de_500()
        {
            decimal venda = 10000;
            decimal comissaoEsperada = 500;

            CalculadoraComissao calculadora = new CalculadoraComissao();
            decimal comissaoCalculada = calculadora.Calcula(venda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void Venda_de_10001_retorna_comissao_de_600_06()
        {
            decimal venda = 10001;
            decimal comissaoEsperada = 600.06M;

            CalculadoraComissao calculadora = new CalculadoraComissao();
            decimal comissaoCalculada = calculadora.Calcula(venda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void Venda_de_55_59_retorna_comissao_de_2_77()
        {
            decimal venda = 55.59M;
            decimal comissaoEsperada = 2.77M;

            CalculadoraComissao calculadora = new CalculadoraComissao();
            decimal comissaoCalculada = calculadora.Calcula(venda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

        [Test]
        public void Venda_de_10000_01_retorna_comissao_de_600()
        {
            decimal venda = 10000.01M;
            decimal comissaoEsperada = 600M;

            CalculadoraComissao calculadora = new CalculadoraComissao();
            decimal comissaoCalculada = calculadora.Calcula(venda);

            Assert.AreEqual(comissaoEsperada, comissaoCalculada);
        }

    }
}