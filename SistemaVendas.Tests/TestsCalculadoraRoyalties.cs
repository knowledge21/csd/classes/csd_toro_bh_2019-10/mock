using System.Collections.Generic;
using Moq;
using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    public class TestesCalculadoraRoyalties
    {
        private Mock<VendaRepository> vendaRepositoryMock;
        private List<Venda> listaVenda;

        [SetUp]
        public void construirMarionetes()
        {
            listaVenda = new List<Venda>();
            vendaRepositoryMock = new Mock<VendaRepository>();
        }

        [Test]
        public void CalculaMesComUnicaVenda()
        {
            int mes = 2;
            int ano = 2019;
            decimal royaltiesEsperados = 1900.0m;

            listaVenda.Add(new Venda(1, 10000));
            vendaRepositoryMock.Setup(m => 
                m.ObterVendasPorMesEAno(mes, ano)
            ).Returns(listaVenda);
           

            CalculadoraRoyalties calculadora = new CalculadoraRoyalties(vendaRepositoryMock.Object, new CalculadoraComissao());
            decimal royaltiesCalculados = calculadora.Calcular(mes, ano);

            Assert.AreEqual(royaltiesEsperados, royaltiesCalculados);
        }

        [Test]
        public void CalculaMesComDuasVendas()
        {
            int mes = 3;
            int ano = 2019;
            decimal royaltiesEsperados = 4156.0m;
            List<Venda> listaVenda = new List<Venda>();
            listaVenda.Add(new Venda(1, 10000));
            listaVenda.Add(new Venda(2, 12000));
            
            Mock<VendaRepository> vendaRepositoryMock = new Mock<VendaRepository>();

            vendaRepositoryMock.Setup(m => 
                m.ObterVendasPorMesEAno(mes, ano)
            ).Returns(listaVenda);
           

            CalculadoraRoyalties calculadora = new CalculadoraRoyalties(vendaRepositoryMock.Object, new CalculadoraComissao());
            
            decimal royaltiesCalculados = calculadora.Calcular(mes, ano);

            Assert.AreEqual(royaltiesEsperados, royaltiesCalculados);
        }
          [Test]
        public void CalculaMesComZeroVendas()
        {
            int mes = 4;
            int ano = 2019;
            decimal royaltiesEsperados = 0m;
            List<Venda> listaVenda = new List<Venda>();
            
            Mock<VendaRepository> vendaRepositoryMock = new Mock<VendaRepository>();

            vendaRepositoryMock.Setup(m => 
                m.ObterVendasPorMesEAno(mes, ano)
            ).Returns(listaVenda);
           

            CalculadoraRoyalties calculadora = new CalculadoraRoyalties(vendaRepositoryMock.Object, new CalculadoraComissao());
            decimal royaltiesCalculados = calculadora.Calcular(mes, ano);

            Assert.AreEqual(royaltiesEsperados, royaltiesCalculados);
        }
    }
}