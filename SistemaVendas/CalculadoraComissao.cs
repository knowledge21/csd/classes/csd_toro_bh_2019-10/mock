using System;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraComissao
    {
        public decimal Calcula(decimal venda)
        {
            decimal comissao;
            if (venda > 10000)
            {
                comissao = venda * 0.06M;
            }
            else
            {
                comissao = venda * 0.05M;
            }
            return Math.Floor(comissao * 100) / 100;
        }
    }
}