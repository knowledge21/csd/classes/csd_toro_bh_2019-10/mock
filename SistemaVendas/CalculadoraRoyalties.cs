﻿﻿using System;
using System.Collections.Generic;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraRoyalties
    {

        private readonly VendaRepository repository;
        private readonly CalculadoraComissao calculadoraComissao;

        public CalculadoraRoyalties(VendaRepository repo, CalculadoraComissao calculadoraComissao) {
            this.repository = repo;
            this.calculadoraComissao = calculadoraComissao;
        }

        public decimal Calcular(int mes, int ano) {

            List<Venda> listaVendas = this.repository.ObterVendasPorMesEAno(mes, ano);
            decimal total = 0m;
            decimal totalComissao = 0;
            foreach (var venda in listaVendas)
            {
                totalComissao += calculadoraComissao.Calcula(venda.Valor);
                total += venda.Valor;
            }

            return (total - totalComissao) * 0.20m;

        }
    }
}